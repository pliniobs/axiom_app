/* *****************************************************************************
 FILE_NAME:     DATALOGGER_Map.h
 DESCRIPTION:   Datalogger map header
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 08/11/2017
 VERSION:       1.0
***************************************************************************** */

#ifndef SOURCES_DATALOG_MAP_H_
#define SOURCES_DATALOG_MAP_H_

#include "APP_INCLUDES.h"

/* *****************************************************************************
 *        WRITE DATALOGGER EXTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
//COPY_SINGLE_VAR(  MeterSerialNum                              , WD     , EXTERN );
//COPY_SINGLE_VAR(  Date                                        , WD     , EXTERN );
//COPY_SINGLE_VAR(  Time                                        , WD     , EXTERN );
//COPY_SINGLE_VAR(  INT_VariableAddress                         , WD     , EXTERN );
//COPY_SINGLE_VAR(  INT_VariableNewValue                        , WD     , EXTERN );
//COPY_SINGLE_VAR(  INT_VariableOldValue                        , WD     , EXTERN );
//
//CREATE_DLOG_MAP(WD, EXTERN);

/* *****************************************************************************
 *        EVENT DATALOGGER EXTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
//COPY_SINGLE_VAR(  MeterSerialNum                              , ED     , EXTERN );
//COPY_SINGLE_VAR(  AlarmCritical_H                             , ED     , EXTERN );
//COPY_SINGLE_VAR(  AlarmCritical_L                             , ED     , EXTERN );
//COPY_SINGLE_VAR(  AlarmError_H                                , ED     , EXTERN );
//COPY_SINGLE_VAR(  AlarmError_L                                , ED     , EXTERN );
//COPY_SINGLE_VAR(  AlarmWarning_H                              , ED     , EXTERN );
//COPY_SINGLE_VAR(  AlarmWarning_L                              , ED     , EXTERN );
//COPY_SINGLE_VAR(  AlarmInfo_H                                 , ED     , EXTERN );
//COPY_SINGLE_VAR(  AlarmInfo_L                                 , ED     , EXTERN );
//COPY_SINGLE_VAR(  Date                                        , ED     , EXTERN );
//COPY_SINGLE_VAR(  Time                                        , ED     , EXTERN );

//CREATE_DLOG_MAP(ED, EXTERN)


#endif
