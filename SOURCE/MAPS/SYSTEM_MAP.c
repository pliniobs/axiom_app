////////////////////////////////////////////////////////
//                                                    //
// FILE_NAME:     SYSTEM_MAP.c                        //
// DESCRIPTION:   APPLICATION SYSTEM TABLES MAP       //
// DESIGNER:      Andre F. N. Dainese                 //
// CREATION_DATE: 03/2015                             //
// TARGET:        Generic                             //
//                                                    //
// VERSION:       1.0                                 //
//                                                    //
////////////////////////////////////////////////////////

/* SETUP.h Defines to import definitions */

#include "APP_INCLUDES.h"
#include "SYSTEM_MAP.h"

/* This file contains configuration tables that informs the SYSTEM_MANAGEMENT */
/*  library some operations that it has to perform.                           */
/* The currently available tables are:                                        */
/* - GPIO Wakeup Table - Which pins awakens the system and how to config them */
/* - GPIO Stay Awake Table - Which pins should be considered when deciding if */
/*    system can go to low power condition and what to check on them.         */
/* - GPIO Sleep Disable Table - Which pins should have their GPIO pins        */
/*    disabled while asleep and reenabled after waking up.                    */
/* - UART Sleep Disable Table - Which UARTs should have their pins            */
/*    disabled while asleep and reenabled after waking up.                    */

/***** GPIO WAKEUP TABLE ******************************************************/


/***** GPIO SLEEP DISABLE TABLE ***********************************************/
#ifdef SYSTEM_MNGT_ENABLE_GPIO_SLEEP_DISABLE_FUNCTIONALITY

const SysMngt_GpioSlpDis_t SysMngt_GpioSleepDisTable[] =
{/*   GPIO ID                                            */
};

const uint8 SYSTEM_MNGT_GPIO_SLPDIS_COUNT = (uint8)(sizeof(SysMngt_GpioSleepDisTable) / sizeof(SysMngt_GpioSlpDis_t));
#endif
/******************************************************************************/

/***** UART SLEEP DISABLE TABLE ***********************************************/
#ifdef SYSTEM_MNGT_ENABLE_UART_SLEEP_DISABLE_FUNCTIONALITY

const SysMngt_UartSlpDis_t SysMngt_UartSleepDisTable[] =
{/*   UART ID                                            */
};

const uint8 SYSTEM_MNGT_UART_SLPDIS_COUNT = (uint8)(sizeof(SysMngt_UartSleepDisTable) / sizeof(SysMngt_UartSlpDis_t));
#endif
/******************************************************************************/
