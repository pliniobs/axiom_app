/* *****************************************************************************
 FILE_NAME:     DATALOGGER_Map.c
 DESCRIPTION:   Datalogger map
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 08/11/2017
 VERSION:       1.0
***************************************************************************** */

/* *****************************************************************************
 *        INCLUDES (AND DEFINES FOR INCLUDES)
***************************************************************************** */
#include "DATALOGGER_Map.h"

#define ENABLE_MEM_RAM_FUNCTIONS
#include "MEM_RAM.h"


/* *****************************************************************************
 *        WRITE DATALOGGER INTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
//COPY_SINGLE_VAR(  MeterSerialNum                              , WD     , INTERN );
//COPY_SINGLE_VAR(  Date                                        , WD     , INTERN );
//COPY_SINGLE_VAR(  Time                                        , WD     , INTERN );
//COPY_SINGLE_VAR(  INT_VariableAddress                         , WD     , INTERN );
//COPY_SINGLE_VAR(  INT_VariableNewValue                        , WD     , INTERN );
//COPY_SINGLE_VAR(  INT_VariableOldValue                        , WD     , INTERN );
//
//CREATE_DLOG_MAP(WD, INTERN)
//{
//               /*---------------------------------------------------------------*/
//               /*  Variable Name                                       , Prefix */
//               /*---------------------------------------------------------------*/
//  ADD_SINGLE_VAR(  MeterSerialNum                                      , WD     ),
//  ADD_SINGLE_VAR(  Date                                                , WD     ),
//  ADD_SINGLE_VAR(  Time                                                , WD     ),
//  ADD_SINGLE_VAR(  INT_VariableAddress                                 , WD     ),
//  ADD_SINGLE_VAR(  INT_VariableNewValue                                , WD     ),
//  ADD_SINGLE_VAR(  INT_VariableOldValue                                , WD     ),
//};


/* *****************************************************************************
 *        EVENT DATALOGGER INTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
//COPY_SINGLE_VAR(  MeterSerialNum                              , ED     , INTERN );
//COPY_SINGLE_VAR(  AlarmCritical_H                             , ED     , INTERN );
//COPY_SINGLE_VAR(  AlarmCritical_L                             , ED     , INTERN );
//COPY_SINGLE_VAR(  AlarmError_H                                , ED     , INTERN );
//COPY_SINGLE_VAR(  AlarmError_L                                , ED     , INTERN );
//COPY_SINGLE_VAR(  AlarmWarning_H                              , ED     , INTERN );
//COPY_SINGLE_VAR(  AlarmWarning_L                              , ED     , INTERN );
//COPY_SINGLE_VAR(  AlarmInfo_H                                 , ED     , INTERN );
//COPY_SINGLE_VAR(  AlarmInfo_L                                 , ED     , INTERN );
//COPY_SINGLE_VAR(  Date                                        , ED     , INTERN );
//COPY_SINGLE_VAR(  Time                                        , ED     , INTERN );
//
//CREATE_DLOG_MAP(ED, INTERN)
//{
//               /*---------------------------------------------------------------*/
//               /*  Variable Name                                       , Prefix */
//               /*---------------------------------------------------------------*/
//  ADD_SINGLE_VAR(   MeterSerialNum                                     , ED     ),
//  ADD_SINGLE_VAR(   AlarmCritical_H                                    , ED     ),
//  ADD_SINGLE_VAR(   AlarmCritical_L                                    , ED     ),
//  ADD_SINGLE_VAR(   AlarmError_H                                       , ED     ),
//  ADD_SINGLE_VAR(   AlarmError_L                                       , ED     ),
//  ADD_SINGLE_VAR(   AlarmWarning_H                                     , ED     ),
//  ADD_SINGLE_VAR(   AlarmWarning_L                                     , ED     ),
//  ADD_SINGLE_VAR(   AlarmInfo_H                                        , ED     ),
//  ADD_SINGLE_VAR(   AlarmInfo_L                                        , ED     ),
//  ADD_SINGLE_VAR(   Date                                               , ED     ),
//  ADD_SINGLE_VAR(   Time                                               , ED     ),
//};
//
///* *****************************************************************************
// *        DATALOGGER MAP INFO TABLE
//***************************************************************************** */
//#if CURRENT_PLATFORM == PLATFORM_DEVELOPMENT
//REFERENCE_MAP =
//{
//             /*--------,--------------------------,-----------------------------,---------,---------,-------------------------------*/
//             /* Prefix , Application ID           , Memory ID                   , Records , Memory  , Type (Periodic / Event / VUR) */
//             /*--------,--------------------------,-----------------------------,---------,---------,-------------------------------*/
//  ADD_DLOG_REF( WD     , DLog_Write               , IWM_01_WD_LOG_EEP_ID        , 8       , MEM_RAM  , TYPE_VUR                      ),
//  ADD_DLOG_REF( ED     , DLog_Event               , IWM_01_ED_LOG_EEP_ID        , 8       , MEM_RAM  , TYPE_EVENT(LogTrigger.Value)  ),
//};
//#else
//REFERENCE_MAP =
//{
//             /*--------,--------------------------,-----------------------------,---------,---------,-------------------------------*/
//             /* Prefix , Application ID           , Memory ID                   , Records , Memory  , Type (Periodic / Event / VUR) */
//             /*--------,--------------------------,-----------------------------,---------,---------,-------------------------------*/
//  ADD_DLOG_REF( WD     , DLog_Write               , IWM_01_WD_LOG_EEP_ID        , 8       , EEPROM  , TYPE_VUR                      ),
//  ADD_DLOG_REF( ED     , DLog_Event               , IWM_01_ED_LOG_EEP_ID        , 8       , EEPROM  , TYPE_EVENT(LogTrigger.Value)  ),
//};
//#endif
//
//const uint8 PTR_MAP_SIZE = (sizeof(DataloggerIDMaps)/sizeof(Dlog_Map_t));
//
