////////////////////////////////////////////////////////
//                                                    //
// FILE_NAME:     SYSTEM_MAP.h                        //
// DESCRIPTION:   APPLICATION SYSTEM TABLES MAP       //
// DESIGNER:      Andre F. N. Dainese                 //
// CREATION_DATE: 02/2016                             //
// TARGET:        Generic                             //
//                                                    //
// VERSION:       1.0                                 //
//                                                    //
////////////////////////////////////////////////////////

#ifndef SYSTEM_MAP_H
#define SYSTEM_MAP_H

#include <stddef.h>
#include "Cpu.h"
#include "GPIO.h"

/* This file selects and defines the configuration tables from the SYS MNGT   */
/*  library. This, along with the source file, defines all the application    */
/*  specific settings that the library should use.                            */
/* The currently available tables are:                                        */
/* - GPIO Wakeup Table - Which pins awakens the system and how to config them */
/* - GPIO Stay Awake Table - Which pins should be considered when deciding if */
/*    system can go to low power condition and what to check on them.         */
/* - GPIO Sleep Disable Table - Which pins should have their GPIO pins        */
/*    disabled while asleep and reenabled after waking up.                    */
/* - UART Sleep Disable Table - Which UARTs should have their pins            */
/*    disabled while asleep and reenabled after waking up.                    */
/* Uncomment their definitions to enable them.                                */

/***** GPIO WAKEUP CONFIGURATION **********************************************/
/* When entering the low power condition the library can configure GPIO pins  */
/*  to be interrupt sources, so that they can awake the system while on sleep */
/*  mode. The table shall have the GPIO ID and the desired interrupt type.    */
/* The library will enable the selected interrupt when entering the low power */
/*  mode and will disable it when leaving the referred mode.                  */
/* The library will deal only with the GPIO's interrupt configuration. All the*/
/*  other properties of them (direction, pull config, etc) are not touched.   */

#define SYSTEM_MNGT_ENABLE_GPIO_WAKEUP_FUNCTIONALITY

#ifdef SYSTEM_MNGT_ENABLE_GPIO_WAKEUP_FUNCTIONALITY
  typedef struct
  {
    uint8           GPIO_ID;
    GPIO_IRQC_TYPE  INT_MODE;
  } SysMngt_GpioWakeup_t;
  
  /* Source file shall provide the table below along with a define that       */
  /*  indicates the number of items in this table.                            */  
  extern const SysMngt_GpioWakeup_t SysMngt_GpioWakeupTable[];  
  extern const uint8 SYSTEM_MNGT_GPIO_WAKEUP_COUNT;
#endif

/***** GPIO STAY AWAKE TABLE **************************************************/
/* If this configuration is enabled, the library will read the pins described */
/*  in the table to check their levels. If the current levels match with the  */
/*  specified one, then system doesn't go to sleep, along with loading a timer*/
/* System will go to sleep only if the pins' levels are different from the    */
/*  specified AND the referred timer expires. Its load value is set below.    */
#define SYSTEM_MNGT_ENABLE_GPIO_STAY_AWAKE_FUNCTIONALITY

#ifdef SYSTEM_MNGT_ENABLE_GPIO_STAY_AWAKE_FUNCTIONALITY
  typedef struct
  {
    uint8 GPIO_ID;
    uint8 STAY_AWAKE_LVL;
  } SysMngt_GpioStayAwake_t;
  
  /* Set below the time, in ms, that all pins shall be in the non-awake value */
  /*  in order to remove the stay awake block.                                */
  #define TIME_STAY_AWAKE_BLOCK       500u
  
  /* Application shall provide the table below along with a define that       */
  /*  indicates the number of items in this table.                            */  
  extern const SysMngt_GpioStayAwake_t SysMngt_GpioStayAwakeTable[];  
  extern const uint8 SYSTEM_MNGT_GPIO_STAYAWAKE_COUNT;
#endif
/******************************************************************************/

/***** GPIO SLEEP DISABLE TABLE ***********************************************/
/* When entering the low power condition the library can change the GPIO pins */
/*  configuration, disabling it. This functionality can be used for, as       */
/*  an example, to to disable a pin's mux (i.e, to change to analog) so that  */
/*  it saves power.                                                           */

//#define SYSTEM_MNGT_ENABLE_GPIO_SLEEP_DISABLE_FUNCTIONALITY

#ifdef SYSTEM_MNGT_ENABLE_GPIO_SLEEP_DISABLE_FUNCTIONALITY
  typedef struct
  {
    uint8                 GPIO_ID;
  } SysMngt_GpioSlpDis_t;
  
  /* Application shall provide the table below along with a define that       */
  /*  indicates the number of items in this table.                            */  
  extern const SysMngt_GpioSlpDis_t SysMngt_GpioSleepDisTable[];  
  extern const uint8 SYSTEM_MNGT_GPIO_SLPDIS_COUNT;

#endif
/******************************************************************************/

/***** UART SLEEP DISABLE TABLE ***********************************************/
/* When entering the low power condition the library can change the UART pins */
/*  configuration, disabling it. This functionality can be used for, as       */
/*  an example, to to disable a pin's mux (i.e, to change to analog) so that  */
/*  it saves power.                                                           */

//#define SYSTEM_MNGT_ENABLE_UART_SLEEP_DISABLE_FUNCTIONALITY

#ifdef SYSTEM_MNGT_ENABLE_UART_SLEEP_DISABLE_FUNCTIONALITY
  typedef struct
  {
    uint8                 UART_ID;
  } SysMngt_UartSlpDis_t;
  
  /* Application shall provide the table below along with a define that       */
  /*  indicates the number of items in this table.                            */  
  extern const SysMngt_UartSlpDis_t SysMngt_UartSleepDisTable[];  
  extern const uint8 SYSTEM_MNGT_UART_SLPDIS_COUNT;

#endif
/******************************************************************************/

#endif
