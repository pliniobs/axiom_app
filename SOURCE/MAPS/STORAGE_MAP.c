/*
 * STORAGE_MAP.c
 *
 *  Created on: 14/06/2016
 *      Author: DENISBERALDO
 */


#include "STORAGE_MAP.h"
#include "SETUP.h"
#include "macros.h"

/* Import Eol variables. */
#include "APP.h"

/* The memory where data will be stored depend of the selected platform.      */
#if CURRENT_PLATFORM == PLATFORM_DEVELOPMENT
/* STORAGE library main map.                                                  */
const Str_Config_Map_t STORAGE_MAP[N_OF_CONTROLLED] =
{
                /*------------,--------------------,--------------------,-----------------,-------------------------*/
                /*  Bank Type , Memory ID's main   , Memory ID's backup , Memory used     , Signature value         */
                /*------------,--------------------,--------------------,-----------------,-------------------------*/
//  ADD_STORAGE_CFG(  NVV_BANK  , STORAGE_ID_NVV_0   , STORAGE_ID_NVV_1   , MEM_RAM         , DEF_SIGNATURE_VAL       ),
//  ADD_STORAGE_CFG(  MDB_BANK  , STORAGE_ID_MDB_0   , STORAGE_ID_MDB_1   , MEM_RAM         , DEF_SIGNATURE_VAL       ),
};
#else
/* STORAGE library main map.                                                  */
const Str_Config_Map_t STORAGE_MAP[N_OF_CONTROLLED] =
{
                /*------------,--------------------,--------------------,-----------------,-------------------------*/
                /*  Bank Type , Memory ID's main   , Memory ID's backup , Memory used     , Signature value         */
                /*------------,--------------------,--------------------,-----------------,-------------------------*/
  ADD_STORAGE_CFG(  NVV_BANK  , STORAGE_ID_NVV_0   , STORAGE_ID_NVV_1   , EEPROM          , DEF_SIGNATURE_VAL       ),
  ADD_STORAGE_CFG(  MDB_BANK  , STORAGE_ID_MDB_0   , STORAGE_ID_MDB_1   , EEPROM          , DEF_SIGNATURE_VAL       ),
};
#endif
