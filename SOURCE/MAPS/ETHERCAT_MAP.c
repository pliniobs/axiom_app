#include "CustomOd.h"
#include "ETHERCAT_MAP.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                 STANDARD OBJECT RxPDO MAPPING - ADDR: 0x1600
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*MAPPING TO RxPDO's DATA                                                     */
static const uint32 s_ab1600_Elements[] =
{
  ADD_PDO_SLOT_MAPPING(0x7000 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7001 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7002 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7003 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7004 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7005 ,0),
};

static  const uint8 s_b1600_NumElements  = ARRCNT(s_ab1600_Elements);

SUBOBJECT_DESCRIPTION_T g_tSiObj_1600[] =
{
                 /*Index, Flag , AccessType     , DataTye                 , MaxField, Name                , InitValue             , IntialValueLength,*/
  ADD_NEW_SUB_OBJ(  0   , 0   , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED8 , 1       , "Number of elements", &s_b1600_NumElements  , sizeof(uint8)   ),

  ADD_SLOT_OBJ(0,s_ab1600_Elements),
//  ADD_SLOT_OBJ(1,s_ab1600_Elements),
//  ADD_SLOT_OBJ(2,s_ab1600_Elements),
//  ADD_SLOT_OBJ(3,s_ab1600_Elements),
//  ADD_SLOT_OBJ(4,s_ab1600_Elements),
//  ADD_SLOT_OBJ(5,s_ab1600_Elements)
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                 STANDARD OBJECT TxPDO MAPPING - ADDR: 0x1A00
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*MAPPING TO TxPDO's DATA*/
static const uint32 s_ab1A00_Elements[] =
{
  ADD_PDO_SLOT_MAPPING(0x7010 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7011 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7012 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7013 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7014 ,0),
//  ADD_PDO_SLOT_MAPPING(0x7015 ,0),
 };

static const uint8_t s_b1A00_NumElements = ARRCNT(s_ab1A00_Elements);

SUBOBJECT_DESCRIPTION_T g_tSiObj_1A00[] =
{
  ADD_NEW_SUB_OBJ(0,0,ECAT_OD_READ_ALL,ECAT_OD_DTYPE_UNSIGNED8,1,"Number of elements",&s_b1A00_NumElements, sizeof(uint8) ),

  ADD_SLOT_OBJ(0,s_ab1A00_Elements),
//  ADD_SLOT_OBJ(1,s_ab1A00_Elements),
//  ADD_SLOT_OBJ(2,s_ab1A00_Elements),
//  ADD_SLOT_OBJ(3,s_ab1A00_Elements),
//  ADD_SLOT_OBJ(4,s_ab1A00_Elements),
//  ADD_SLOT_OBJ(5,s_ab1A00_Elements)
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                 SLOT PDO OBJECT - ADDR: 0x7000
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static const TLR_UINT8 s_b7000_NumElements;

SUBOBJECT_DESCRIPTION_T g_tSiObj_7000[] =
{
  ADD_NEW_SUB_OBJ(0,0,ECAT_OD_READ_ALL,ECAT_OD_DTYPE_UNSIGNED8,1,"Number of elements",&s_b7000_NumElements, sizeof(uint8) ),
                 /*Index, Flag ,    AccessType      ,     DataTye            ,MaxField,    Name       , InitValue , IntialValueLength,*/
  ADD_NEW_SUB_OBJ(   1  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"FRAME TYPE"   ,     0    ,       0       ),
  ADD_NEW_SUB_OBJ(   2  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"DEVICE ID MSB",     0    ,       0       ),
  ADD_NEW_SUB_OBJ(   3  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"DEVICE ID LSB",     0    ,       0       ),
  ADD_NEW_SUB_OBJ(   4  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"SLOT"         ,     0    ,       0       ),
  ADD_NEW_SUB_OBJ(   5  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"LENGTH MSB"   ,     0    ,       0       ),
  ADD_NEW_SUB_OBJ(   6  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"LENGTH LSB"   ,     0    ,       0       ),
  ADD_NEW_SUB_OBJ(   7  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"GENERIC DATA",     0    ,        0       ),
  ADD_NEW_SUB_OBJ(   8  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"GENERIC DATA",     0    ,        0       ),
  ADD_NEW_SUB_OBJ(   9  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"GENERIC DATA",     0    ,        0       ),
  ADD_NEW_SUB_OBJ(   10  ,  0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"GENERIC DATA",     0    ,        0       ),

  ADD_NEW_SUB_OBJ(   11  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   12  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   13  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   14  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   15  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   16  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   17  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   18  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   19  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   20  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),

  ADD_NEW_SUB_OBJ(   21  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   22  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   23  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   24  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   25  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   26  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   27  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   28  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   29  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   30  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),

  ADD_NEW_SUB_OBJ(   31  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   32  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   33  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   34  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   35  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   36  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   37  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   38  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   39  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
  ADD_NEW_SUB_OBJ(   40  ,   0  ,  ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8,   1  ,"GENERIC DATA",     0     ,        0        ),
};

static const TLR_UINT8 s_b7000_NumElements = ARRCNT(g_tSiObj_7000) - 1; /* SI 00 does not counts */

static const TLR_UINT8 s_bBooleanInitTrueValue = 1;
static const TLR_UINT32 s_ulValue12345678      = 0;
static const TLR_UINT16 DeviceIdMsdc           = 0x08;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *          STANDARD OBJECT SYNC MANAGER COMM - ADDR: 0x1C00
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static const TLR_UINT8 s_ab1C00_Elements[] = {1, 2, 3, 4};
static const TLR_UINT8 s_b1C00_NumElements = ARRCNT(s_ab1C00_Elements);

SUBOBJECT_DESCRIPTION_T g_tSiObj_1C00[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b1C00_NumElements, sizeof(uint8) ),

  /*Index, Flag ,    AccessType    ,     DataTye            ,MaxField,    Name        ,  InitValue             , IntialValueLength,*/
  ADD_NEW_SUB_OBJ(   1  ,   0  ,  ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"Sync Manager 0",  &s_ab1C00_Elements[0] , sizeof(uint8)   ),
  ADD_NEW_SUB_OBJ(   2  ,   0  ,  ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"Sync Manager 1",  &s_ab1C00_Elements[1] , sizeof(uint8)   ),
  ADD_NEW_SUB_OBJ(   3  ,   0  ,  ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"Sync Manager 2",  &s_ab1C00_Elements[2] , sizeof(uint8)   ),
  ADD_NEW_SUB_OBJ(   4  ,   0  ,  ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED8,    1   ,"Sync Manager 3",  &s_ab1C00_Elements[3] , sizeof(uint8)   ),
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *          ??????  - ADDR: 0x1C12
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


static const TLR_UINT16 s_aus1C12_Entries[] = { 0x1600 };
static const TLR_UINT8 s_b1C12_NumElements = ARRCNT(s_aus1C12_Entries);

SUBOBJECT_DESCRIPTION_T tSiObj_1C12[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b1C12_NumElements, sizeof(uint8) ),

  /*Index, Flag ,    AccessType    ,     DataTye            ,MaxField,    Name        ,  InitValue          , IntialValueLength,*/
  ADD_NEW_SUB_OBJ(1     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED16 ,   1   ,      0         ,&s_aus1C12_Entries[0],  sizeof(uint16) ),
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *          ??????  - ADDR: 0x1C13
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static const TLR_UINT16 s_aus1C13_Entries[] = { 0x1A00 };
static const TLR_UINT8  s_b1C13_NumElements = ARRCNT(s_aus1C13_Entries);

SUBOBJECT_DESCRIPTION_T g_tSiObj_1C13[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b1C13_NumElements, sizeof(uint8) ),

  /*Index, Flag ,    AccessType    ,     DataTye            ,MaxField,    Name        ,  InitValue          , IntialValueLength,*/
  ADD_NEW_SUB_OBJ(1     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED16 ,   1   ,      0         ,&s_aus1C13_Entries[0],  sizeof(uint16) ),
};


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                     OBJECT SLOT - ADDR: 0x3008 - 0x3014
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
static const TLR_UINT8 s_b3008_NumElements;

SUBOBJECT_DESCRIPTION_T g_tSiObj_3008[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b3008_NumElements, sizeof(uint8) ),

                 /*Index, Flag ,    AccessType    ,     DataTye                ,MaxField,    Name      ,  InitValue  , IntialValueLength,*/
  ADD_NEW_SUB_OBJ(1     ,   0  ,ECAT_OD_ACCESS_ALL,ECAT_OD_DTYPE_UNSIGNED16   ,   1   , "DEVICE ID"    ,&DeviceIdMsdc,  sizeof(uint16) ),
  ADD_NEW_SUB_OBJ(2     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED16   ,   1   , "DIAGNOSTIC"   ,    0        ,         0       ),
  ADD_NEW_SUB_OBJ(3     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED16   ,   1   , "ALARM"        ,    0        ,         0       ),
  ADD_NEW_SUB_OBJ(4     ,   0  ,ECAT_OD_ACCESS_ALL,ECAT_OD_DTYPE_UNSIGNED32   ,   1   , "MDL REQUEST"  ,    0        ,         0       ),
  ADD_NEW_SUB_OBJ(5     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8    ,   1   , "MDL RETURN"   ,    0        ,         0       ),
  ADD_NEW_SUB_OBJ(6     ,   0  ,ECAT_OD_ACCESS_ALL,ECAT_OD_DTYPE_UNSIGNED32   ,   1   , "CONFIGURATION",    0        ,         0       ),


};

static const TLR_UINT8 s_b3008_NumElements = ARRCNT(g_tSiObj_3008) - 1; /* SI 00 does not counts */


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                     OBJECT ARM - ADDR: 0x4000 - 0x4001
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static const TLR_UINT8 s_b4000_NumElements;

SUBOBJECT_DESCRIPTION_T g_tSiObj_4000[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b4000_NumElements, sizeof(uint8) ),

                 /*Index, Flag ,    AccessType    ,     DataTye            ,MaxField,    Name          , InitValue, IntialValueLength,*/
  ADD_NEW_SUB_OBJ(1     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED32,   1    , "LINES"           , 0        , 0               ),
  ADD_NEW_SUB_OBJ(2     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "ARM TYPE"        , 0        , 0               ),
  ADD_NEW_SUB_OBJ(3     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "PRESENT"         , 0        , 0               ),
  ADD_NEW_SUB_OBJ(4     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "ENABLE"          , 0        , 0               ),
  ADD_NEW_SUB_OBJ(5     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "LINE QUANTITY"   , 0        , 0               ),
  ADD_NEW_SUB_OBJ(6     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "ALGORITHM"       , 0        , 0               ),
  ADD_NEW_SUB_OBJ(7     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "REQUEST"         , 0        , 0               ),
  ADD_NEW_SUB_OBJ(8     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "REQUEST ANSWER"  , 0        , 0               ),
  ADD_NEW_SUB_OBJ(9     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED16,   1    , "USER ID"         , 0        , 0               ),
  ADD_NEW_SUB_OBJ(10    ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED16,   1    , "PRESET QUANTITY" , 0        , 0               ),
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                     OBJECT LINE - ADDR: 0x5000 - 0x5005
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static const TLR_UINT8 s_b5000_NumElements;

SUBOBJECT_DESCRIPTION_T g_tSiObj_5000[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b5000_NumElements, sizeof(uint8) ),

                 /*Index, Flag ,    AccessType    ,     DataTye            ,MaxField,    Name            , InitValue, IntialValueLength,*/
  ADD_NEW_SUB_OBJ(1     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED32,   1    , "METERS"          , 0        , 0               ),
  ADD_NEW_SUB_OBJ(2     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_BOOLEAN   ,   1    , "LINE TYPE"       , 0        , 0               ),
  ADD_NEW_SUB_OBJ(3     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_BOOLEAN   ,   1    , "PRESENT"         , 0        , 0               ),
  ADD_NEW_SUB_OBJ(4     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_BOOLEAN   ,   1    , "ENABLE"          , 0        , 0               ),
  ADD_NEW_SUB_OBJ(5     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "METER QUANTITY"  , 0        , 0               ),
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                     OBJECT METER - ADDR: 0x5006 - 0x5018
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static const TLR_UINT8 s_b5006_NumElements;

SUBOBJECT_DESCRIPTION_T g_tSiObj_5006[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b5006_NumElements, sizeof(uint8) ),

                 /*Index, Flag ,    AccessType    ,     DataTye            ,MaxField,    Name            , InitValue, IntialValueLength,*/
  ADD_NEW_SUB_OBJ(1     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED32,   1    , "METERS TYPE"     , 0        , 0               ),
  ADD_NEW_SUB_OBJ(2     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "SLOT"            , 0        , 0               ),
  ADD_NEW_SUB_OBJ(3     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED8 ,   1    , "CHANNEL"         , 0        , 0               ),
  ADD_NEW_SUB_OBJ(4     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_BOOLEAN   ,   1    , "PRESENT"         , 0        , 0               ),
  ADD_NEW_SUB_OBJ(5     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_BOOLEAN   ,   1    , "ENABLE"          , 0        , 0               ),
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                     OBJECT LOG ACCESS - ADDR: 0x6050
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static const TLR_UINT8 s_b6050_NumElements;

SUBOBJECT_DESCRIPTION_T g_tSiObj_6050[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b6050_NumElements, sizeof(uint8) ),

                 /*Index, Flag ,    AccessType    ,     DataTye             ,MaxField,    Name            , InitValue, IntialValueLength,*/
  ADD_NEW_SUB_OBJ(1     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED16,   1    , "LOG STATUS"       , 0        , 0               ),
  ADD_NEW_SUB_OBJ(2     ,   0  ,ECAT_OD_READ_ALL  , ECAT_OD_DTYPE_UNSIGNED16,   1    , "LOG REQUEST"      , 0        , 0               ),
  ADD_NEW_SUB_OBJ(3     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED16,   1    , "REGISTER QUANTITY", 0        , 0               ),
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                     OBJECT LOG ACCESS - ADDR: 0x6000
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static const TLR_UINT8 s_b6000_NumElements;

SUBOBJECT_DESCRIPTION_T g_tSiObj_6000[] =
{
  ADD_NEW_SUB_OBJ(0     ,   0  ,ECAT_OD_READ_ALL  ,ECAT_OD_DTYPE_UNSIGNED8 ,    1   ,"Number of elements",&s_b6000_NumElements, sizeof(uint8) ),

                 /*Index, Flag ,    AccessType    ,     DataTye            ,MaxField,    Name            , InitValue, IntialValueLength,*/
  ADD_NEW_SUB_OBJ(1     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_VISIBLE_STRING,   1    , "FW FILENAME"     , 0        , 0               ),
  ADD_NEW_SUB_OBJ(2     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED32    ,   1    , "DEVICE ID"       , 0        , 0               ),
  ADD_NEW_SUB_OBJ(3     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED16    ,   1    , "FW VERSION"      , 0        , 0               ),
  ADD_NEW_SUB_OBJ(4     ,   0  ,ECAT_OD_READ_ALL  , ECAT_OD_DTYPE_UNSIGNED16    ,   1    , "FW STATUS"       , 0        , 0               ),
  ADD_NEW_SUB_OBJ(5     ,   0  ,ECAT_OD_ACCESS_ALL, ECAT_OD_DTYPE_UNSIGNED16    ,   1    , "FW REQUEST"      , 0        , 0               ),
};

static const TLR_UINT8 s_b6000_NumElements = ARRCNT(g_tSiObj_6000);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                     OBJECT DICTIONARY OF PRODUCT
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

OBJECT_DESCRIPTION_T ETHERCAT_OD_MAP[] =
{

#ifndef DEVELOPMENT_OD
  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   *                     STANDARD ETHERCAT OBJs
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  /*         (Index ,  MaxNumSubObj             , ObjType             , AccessFlag     , IndFlag     , DataType                 , AccessRight      , MaxFieldUnits , Name                              , ptSubObbj        , ptSubObjBreak                         ) , */
  ADD_NEW_OBJ(0x1600,  ARRCNT(s_ab1600_Elements), ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_PDO_MAPPING, ECAT_OD_READ_ALL , 0x00          , "1. RxPDO"                        , &g_tSiObj_1600[0], &g_tSiObj_1600[ ARRCNT(g_tSiObj_1600)]) ,

  ADD_NEW_OBJ(0x1A00,  ARRCNT(s_ab1A00_Elements), ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_PDO_MAPPING, ECAT_OD_READ_ALL , 0x00          , "1. TxPDO"                        , &g_tSiObj_1A00[0], &g_tSiObj_1A00[ ARRCNT(g_tSiObj_1A00)]) ,

  ADD_NEW_OBJ(0x1C00,  0x04                     , ODV3_OBJCODE_ARRAY  , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL , 0x00          , "Sync Manager Communication Types", &g_tSiObj_1C00[0], &g_tSiObj_1C00[ ARRCNT(g_tSiObj_1C00)]) ,

  ADD_NEW_OBJ(0x1C10,  0x00                     , ODV3_OBJCODE_ARRAY  , FLAG_1C10      , NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED16 , ECAT_OD_READ_ALL , 0x01          , "Sync Manager 0 PDO Assignment"   , PT_VOID          , PT_VOID                               ) ,

  ADD_NEW_OBJ(0x1C11,  0x00                     , ODV3_OBJCODE_ARRAY  , FLAG_1C11      , NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED16 , ECAT_OD_READ_ALL , 0x01          , "Sync Manager 1 PDO Assignment"   , PT_VOID          , PT_VOID                               ) ,

  ADD_NEW_OBJ(0x1C12,  0x01                     , ODV3_OBJCODE_ARRAY  , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED16 , ECAT_OD_READ_ALL , 0x01          , "Sync Manager 1 PDO Assignment"   , &tSiObj_1C12[0]  , &tSiObj_1C12[ ARRCNT(tSiObj_1C12) ]   ) ,

  ADD_NEW_OBJ(0x1C13,  0x01                     , ODV3_OBJCODE_ARRAY  , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED16 , ECAT_OD_READ_ALL , 0x00          , "Sync Manager 1 PDO Assignment"   , &g_tSiObj_1C13[0]  , &g_tSiObj_1C13[ ARRCNT(g_tSiObj_1C13) ]   ) ,

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   *                     AXIOM OD
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  /*SLOTs                                                                    */

  //  ADD_NEW_OBJ(0x3008,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS,  ODV3_INDICATION_FLAGS_ON_READ | ODV3_INDICATION_FLAGS_ON_WRITE, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL  , 0x01         , "SLOT 0"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x3008,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS,  NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 0"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,

  ADD_NEW_OBJ(0x300A,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 1"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x300B,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 2"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x300C,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 3"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x300D,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 4"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x300E,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 5"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x300F,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 6"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x3010,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 7"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x3011,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 8"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x3012,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 9"                          , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  ADD_NEW_OBJ(0x3013,  ARRCNT(g_tSiObj_3008)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_READ_ALL  , 0x01         , "SLOT 10"                         , &g_tSiObj_3008[0], &g_tSiObj_3008[ ARRCNT(g_tSiObj_3008)]) ,
  /* METROLOGICAL                                                            */
  ADD_NEW_OBJ(0x4000,  ARRCNT(g_tSiObj_4000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "ARM 0"                           , &g_tSiObj_4000[0], &g_tSiObj_4000[ ARRCNT(g_tSiObj_4000)]) ,
  ADD_NEW_OBJ(0x4001,  ARRCNT(g_tSiObj_4000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "ARM 1"                           , &g_tSiObj_4000[0], &g_tSiObj_4000[ ARRCNT(g_tSiObj_4000)]) ,

  ADD_NEW_OBJ(0x5000,  ARRCNT(g_tSiObj_5000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "LINE 0"                          , &g_tSiObj_5000[0], &g_tSiObj_5000[ ARRCNT(g_tSiObj_5000)]) ,
  ADD_NEW_OBJ(0x5001,  ARRCNT(g_tSiObj_5000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "LINE 1"                          , &g_tSiObj_5000[0], &g_tSiObj_5000[ ARRCNT(g_tSiObj_5000)]) ,
  ADD_NEW_OBJ(0x5002,  ARRCNT(g_tSiObj_5000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "LINE 2"                          , &g_tSiObj_5000[0], &g_tSiObj_5000[ ARRCNT(g_tSiObj_5000)]) ,
  ADD_NEW_OBJ(0x5003,  ARRCNT(g_tSiObj_5000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "LINE 3"                          , &g_tSiObj_5000[0], &g_tSiObj_5000[ ARRCNT(g_tSiObj_5000)]) ,
  ADD_NEW_OBJ(0x5004,  ARRCNT(g_tSiObj_5000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "LINE 4"                          , &g_tSiObj_5000[0], &g_tSiObj_5000[ ARRCNT(g_tSiObj_5000)]) ,
  ADD_NEW_OBJ(0x5005,  ARRCNT(g_tSiObj_5000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "LINE 5"                          , &g_tSiObj_5000[0], &g_tSiObj_5000[ ARRCNT(g_tSiObj_5000)]) ,

  ADD_NEW_OBJ(0x5006,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 0"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5007,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 1"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5008,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 2"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5009,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 3"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x500A,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 4"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x500B,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 5"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x500C,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 6"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x500D,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 7"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x500E,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 8"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x500F,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 9"                         , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5010,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 10"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5011,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 11"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5012,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 12"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5013,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 13"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5014,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 14"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5015,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 15"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5016,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 16"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5017,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 17"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,
  ADD_NEW_OBJ(0x5018,  ARRCNT(g_tSiObj_5006)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "METER 18"                        , &g_tSiObj_5006[0], &g_tSiObj_5006[ ARRCNT(g_tSiObj_5006)]) ,

  ADD_NEW_OBJ(0x6000,  ARRCNT(g_tSiObj_6000)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "FIRMWARE ACCESS"                 , &g_tSiObj_6000[0], &g_tSiObj_6000[ ARRCNT(g_tSiObj_6000)]) ,
  ADD_NEW_OBJ(0x6050,  ARRCNT(g_tSiObj_6050)-1  , ODV3_OBJCODE_RECORD , NO_ACCESS_FLAGS, NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x01         , "LOG ACCESS"                      , &g_tSiObj_6050[0], &g_tSiObj_6050[ ARRCNT(g_tSiObj_6050)]) ,

  ADD_NEW_OBJ(0x7000,  ARRCNT(g_tSiObj_7000)-1    , ODV3_OBJCODE_RECORD , FLAG_2000         , NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x00      , "PDO SLOT OUT 0"               , &g_tSiObj_7000[0], &g_tSiObj_7000[ ARRCNT(g_tSiObj_7000)]) ,
  ADD_NEW_OBJ(0x7001,  ARRCNT(g_tSiObj_7000)-1    , ODV3_OBJCODE_RECORD , FLAG_2000         , NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x00      , "PDO SLOT OUT 0"               , &g_tSiObj_7000[0], &g_tSiObj_7000[ ARRCNT(g_tSiObj_7000)]) ,

  ADD_NEW_OBJ(0x7010,  ARRCNT(g_tSiObj_7000)-1    , ODV3_OBJCODE_ARRAY  ,  FLAG_3000  , NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x00            , "PDO SLOT IN  0"               , &g_tSiObj_7000[0], &g_tSiObj_7000[ ARRCNT(g_tSiObj_7000)]) ,
  ADD_NEW_OBJ(0x7011,  ARRCNT(g_tSiObj_7000)-1    , ODV3_OBJCODE_ARRAY  ,  FLAG_3000  , NO_IND_FLAGS, ECAT_OD_DTYPE_UNSIGNED8  , ECAT_OD_ACCESS_ALL, 0x00            , "PDO SLOT IN  0"               , &g_tSiObj_7000[0], &g_tSiObj_7000[ ARRCNT(g_tSiObj_7000)]) ,
#endif

};

CUSTOM_OD_T g_customOd =
{
  .ptCurrObj    = &ETHERCAT_OD_MAP[0]                   ,
  /* pointer to one after last object which is taken as break condition in a loop */
  .ptBreakObj   = &ETHERCAT_OD_MAP[ ARRCNT(ETHERCAT_OD_MAP) ],
  .ptCurrSubObj = 0                                ,
};
