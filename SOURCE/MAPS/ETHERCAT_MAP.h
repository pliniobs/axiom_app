#ifndef ETHERCAT_MAP_H_
#define ETHERCAT_MAP_H_

#define ADD_PDO_SLOT_MAPPING(INDEX,SLOT_NUMBER)                    \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER + 1, 8),                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +2, 8) ,                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +3, 8) ,                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +4, 8) ,                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +5, 8) ,                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +6, 8) ,                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +7, 8) ,                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +8, 8) ,                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +9, 8) ,                        \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +10, 8),                        \
                                                                   \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +11, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +12, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +13, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +14, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +15, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +16, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +17, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +18, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +19, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +20, 8) ,                       \
                                                                   \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +21, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +22, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +23, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +24, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +25, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +26, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +27, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +28, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +29, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +30, 8) ,                       \
                                                                   \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +31, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +32, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +33, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +34, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +35, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +36, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +37, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +38, 8) ,                       \
  PDOMAPPING(INDEX, 39*SLOT_NUMBER +39, 8)


#define ADD_SLOT_OBJ(SLOT_NUMBER,MAP_VAR)                                 \
  ADD_NEW_SUB_OBJ(1 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 0 +39*SLOT_NUMBER , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(2 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 1 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(3 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 2 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(4 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 3 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(5 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 4 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(6 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 5 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(7 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 6 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(8 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 7 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(9 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 8 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(10 +39*SLOT_NUMBER   , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   ,  MAP_VAR + 9 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(11 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 10 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(12 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 11 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(13 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 12 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(14 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 13 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(15 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 14 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(16 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 15 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(17 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 16 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(18 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 17 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(19 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 18 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(20 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 19 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
\
  ADD_NEW_SUB_OBJ(21 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 20 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(22 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 21 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(23 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 22 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(24 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 23 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(25 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 24 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(26 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 25 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(27 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 26 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(28 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 27 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(29 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 28 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(30 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 29  +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(31 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 30 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(32 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 31 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(33 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 32 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(34 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 33 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(35 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 34 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(36 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 35 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(37 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 36 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(38 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 37 +39*SLOT_NUMBER  , sizeof(uint32)  ), \
  ADD_NEW_SUB_OBJ(39 +39*SLOT_NUMBER    , 0  , ECAT_OD_READ_ALL, ECAT_OD_DTYPE_UNSIGNED32 , 1   , 0   , MAP_VAR + 38 +39*SLOT_NUMBER  , sizeof(uint32)  )  \


#define TEST(I)                                       \
  ADD_NEW_SUB_OBJ(   1 +40*I ,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"FRAME TYPE"   ,     0    ,       0       ),\
  ADD_NEW_SUB_OBJ(   2 +40*I ,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO,  ECAT_OD_DTYPE_BYTE,   1   ,"DEVICE ID MSB",     0    ,       0       ),\
  ADD_NEW_SUB_OBJ(   3 +40*I ,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"DEVICE ID LSB",     0    ,       0       ),\
  ADD_NEW_SUB_OBJ(   4 +40*I ,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"SLOT"         ,     0    ,       0       ),\
  ADD_NEW_SUB_OBJ(   5 +40*I ,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"LENGTH MSB"   ,     0    ,       0       ),\
  ADD_NEW_SUB_OBJ(   6  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"LENGTH LSB"   ,     0    ,       0       ),\
  ADD_NEW_SUB_OBJ(   7  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"GENERIC DATA",     0    ,        0       ),\
  ADD_NEW_SUB_OBJ(   8  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"GENERIC DATA",     0    ,        0       ),\
  ADD_NEW_SUB_OBJ(   9  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"GENERIC DATA",     0    ,        0       ),\
  ADD_NEW_SUB_OBJ(   10  +40*I,  0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,    1   ,"GENERIC DATA",     0    ,        0       ),\
\
  ADD_NEW_SUB_OBJ(   11  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   12  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   13  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   14  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   15  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   16  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   17  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   18  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   19  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   20  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
\
  ADD_NEW_SUB_OBJ(   21  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   22  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   23  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   24  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   25  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   26  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   27  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   28  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   29  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   30  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
\
  ADD_NEW_SUB_OBJ(   31  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   32  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   33  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   34  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   35  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   36  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   37  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   38  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   39  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\
  ADD_NEW_SUB_OBJ(   40  +40*I,   0  ,  ECAT_OD_ACCESS_ALL | ECAT_OD_MAPPABLE_IN_RXPDO, ECAT_OD_DTYPE_BYTE,   1  ,"GENERIC DATA",     0     ,        0        ),\


#define FLAG_1C10        ODV3_ACCESS_FLAGS_CREATE_SUBINDEX_0 | ODV3_ACCESS_FLAGS_FORCE_INDEXED
#define FLAG_1C11        ODV3_ACCESS_FLAGS_CREATE_SUBINDEX_0 | ODV3_ACCESS_FLAGS_FORCE_INDEXED

#define FLAG_2000                              ODV3_ACCESS_FLAGS_RXPDO_MAPPABLE
#define FLAG_3000                              ODV3_ACCESS_FLAGS_TXPDO_MAPPABLE

#define NO_IND_FLAGS                                                       0x00//ODV3_INDICATION_FLAGS_ON_READ | ODV3_INDICATION_FLAGS_ON_WRITE
// 0x00
#define NO_ACCESS_FLAGS                                                    0x00

#define PT_VOID                                                            0x00

#endif
