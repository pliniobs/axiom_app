/* *****************************************************************************
 *        INCLUDES (and DEFINES for INCLUDES)
 ***************************************************************************** */
#define IMPORT_SETUP_DEBUG

#include "SETUP.h"
#include "ETHERCAT_Link.h"

#include "types.h"
#include "Setup.h"
#include "macros.h"
#include <string.h>
#include "Debug.h"

#include "APP_INCLUDES.h"

/* *****************************************************************************
 *
 *                FUNCTIONS
 *
 ******************************************************************************/
static void RunLibraries(void);
//static void RunApplication(void);

/* *****************************************************************************
 *
 *                DEFINES, ENUMS, STRUCT
 *
 ******************************************************************************/

/* *****************************************************************************
 *
 *                VARIABLES, TABLES
 *
 ******************************************************************************/
int main(void)
{
  while(1)
  {
    ////////////////////////////////////////
    // Runs the libraries of the system   //
    // based on the current functioning   //
    // mode                               //
    ////////////////////////////////////////
    RunLibraries();

    ////////////////////////////////////////
    // Execute all the specific routines  //
    // of the application                 //
    ////////////////////////////////////////
//    RunApplication();

  }
}

//////////////////////////////////////////////////////////////////
// Run every library from the product and gather their answers  //
//////////////////////////////////////////////////////////////////
static void RunLibraries(void)
{
  ETHERCAT();
}

