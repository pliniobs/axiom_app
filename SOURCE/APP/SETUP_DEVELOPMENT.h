#ifndef SETUP_DEVELOPMENT_H_INCLUDE
#define SETUP_DEVELOPMENT_H_INCLUDE

/******************************************************************************/
/*        DEFINES, ENUMS, STRUCTURES                                          */
/******************************************************************************/
/* GPIO pins                                                                  */
#define ACCEL_INT_PORT                                                    PORT_B
#define ACCEL_INT_PIN                                                     PIN_15

#define MAGNET_1_PORT                                                     PORT_B
#define MAGNET_1_PIN                                                       PIN_5

#define MAGNET_2_PORT                                                     PORT_B
#define MAGNET_2_PIN                                                       PIN_7

#define WIRELESS_FAULT_PORT                                               PORT_B
#define WIRELESS_FAULT_PIN                                                 PIN_6

#define WIRELESS_EVENT_PORT                                               PORT_A
#define WIRELESS_EVENT_PIN                                                 PIN_9

#define USB_CONNECTION_PORT                                               PORT_B
#define USB_CONNECTION_PIN                                                PIN_13

#define BOARD_LORA_RESET_PORT                                             PORT_C
#define BOARD_LORA_RESET_PIN                                               PIN_0

#define BOARD_HF_READER_ON_PORT                                           PORT_A
#define BOARD_HF_READER_ON_PIN                                             PIN_8

#define BOARD_LED_RED_PORT                                                PORT_A
#define BOARD_LED_RED_PIN                                                  PIN_5

#define BOARD_LED_GREEN_PORT                                              PORT_B
#define BOARD_LED_GREEN_PIN                                                PIN_5

#define BOARD_LED_BLUE_PORT                                               PORT_B
#define BOARD_LED_BLUE_PIN                                                 PIN_6

/* UART Channels                                                              */
#define SERIAL_USB_SERVICE_INT                                   UART2_INTERFACE
#define SERIAL_USB_SERVICE_UART                           UART2_AT_PA3_PA2_RX_TX

#define SERIAL_HF_READER_INT                                     UART1_INTERFACE
#define SERIAL_HF_READER_UART                            UART1_AT_PA10_PA9_RX_TX

/* Analog pins                                                                */

/* IIC Channels                                                               */
#define I2C_BUS_INT                               IIC_ROUTE_IIC1_PB8_SCL_PB9_SDA
#define I2C_BUS_SDA                                                SDA0_AT_PTE25 /* Don't care for STM32L0 */
#define I2C_BUS_SCL                                                SCL0_AT_PTE24 /* Don't care for STM32L0 */

/* SPI Channels / CS Pins                                                     */
#define BOARD_LORA_SPI                                    SPI1_ROUTE_PA7_PA6_PB3
#define BOART_LORA_CS_PORT                                                 GPIOA
#define BOART_LORA_CS_PIN                                                     15

/******************************************************************************/
/*        MODBUS MASTER SPECIFIC CONFIGURATION                                */
/******************************************************************************/
/* Channel-Related Configuration:                                             */
/*----------------------------------------------------------------------------*/
/*                                                                   CHANNEL 0*/
/*----------------------------------------------------------------------------*/
/* UART Configuration - Not used in this project.                             */
#define MDBM_UART_ID_LIST               (uint8)                         DONTCARE
#define MDBM_UART_INTERFACE_LIST        (PortOptions)                   DONTCARE
#define MDBM_UART_ROUTED_PORT_LIST      (RoutedPortOptions)             DONTCARE
#define MDBM_UART_BAUDRATE_LIST         (BaudRateOptions)               DONTCARE

/* LORA Configuration                                                         */
#define MDB_MASTER_LORA_SUPPORT                                             TRUE
#define MDBM_LORA_ID_LIST                               IWM_01_MDBM_LORA_ID_LIST

/* PIT IDs used for the transactions' control                                 */
#define MDBM_BUS_PIT_ID_LIST                         IWM_01_MDBM_BUS_PIT_ID_LIST

/* Select below the communication mode for each channel.                      */
#define MDBM_COMM_MODE_LIST             (MdbMst_CommMode_t)       MDBM_MODE_LORA

/* Select below the frame format for each channel.                            */
#define MDBM_FRAME_FORMAT               (MdbMst_Format_t)     MDBM_FRAME_REGULAR

/* GPIO Configurations - OUT Pin may be the Control or RTS pin. It'll depend  */
/*  on the communication mode set for the channel.                            */
#define MDBM_OUTP_GPIO_ID_LIST          (uint8)                         DONTCARE
#define MDBM_OUTP_GPIO_PORT_LIST        (GPIO_PORT_list)                DONTCARE
#define MDBM_OUTP_GPIO_PIN_LIST         (GPIO_PIN_list)                 DONTCARE
#define MDBM_INPT_GPIO_ID_LIST          (uint8)                         DONTCARE
#define MDBM_INPT_GPIO_PORT_LIST        (GPIO_PORT_list)                DONTCARE
#define MDBM_INPT_GPIO_PIN_LIST         (GPIO_PIN_list)                 DONTCARE

/* Channel-Independent Configuration:                                         */
/* Define below the PIT ID that will be used for the library period control   */
#define MDBM_PERIOD_PIT_ID                             IWM_01_MDBM_PERIOD_PIT_ID

/* Define below the library's period for each step, in mili seconds.          */
/* Warning: The lists' periods should be multiple of this value!              */
#define MDBM_PERIOD_TIME_VALUE          (uint32)                            100u  /* ms */

/* Define below the default value for the reception timeout configuration.    */
#define MDBMST_TIMEOUT_DEF_MS           (uint32)                           5000u  /* ms */

/* Functionalities Enable:                                                    */
/* No special functionalities are needed for this project.                    */
#define MDBM_ENABLE_MODBUS_BRIDGE                                          FALSE
#define MDBM_ENABLE_EXTERNAL_CFG                                           FALSE

/* Slave limit selection:                                                     */
/* This project will deal with only one slave.                                */
#define MDB_MASTER_SLAVE_LIMIT                                                 1

/******************************************************************************/
/*        MODBUS SLAVE SPECIFIC CONFIGURATION                                 */
/******************************************************************************/
/* Enable LoRa support                                                        */
#define MDBS_INCLUDE_LORA_DEV                                               FALSE

/* Quantity of devices managed to the */
/* library                            */
#define MDBS_DEVICE_AMOUNT                  (uint8)                            2

                                          /*------------------------------------,------------------------------------*/
/* Limits used at "MDBS_DeviceAddress"*/  /*                          CHANNEL 0 ,                          CHANNEL 1 */
/* variable declaration               */  /*------------------------------------,------------------------------------*/
#define MDBS_MIN_ADDR_LIST                  (uint8)                            1, (uint8)                           1
#define MDBS_INI_ADDR_LIST                  (uint8)                            1, (uint8)                           1
#define MDBS_MAX_ADDR_LIST                  (uint8)                          247, (uint8)                         247

                                          /*------------------------------------,------------------------------------*/
/* Limits used at "MDBS_BaudRate"     */  /*                          CHANNEL 0 ,                          CHANNEL 1 */
/* variable declaration               */  /*------------------------------------,------------------------------------*/
#define MDBS_MIN_BAUD_LIST                  (BaudRateOptions)       UART_9600bps, (BaudRateOptions)      UART_9600bps
#define MDBS_INI_BAUD_LIST                  (BaudRateOptions)      UART_57600bps, (BaudRateOptions)     UART_57600bps
#define MDBS_MAX_BAUD_LIST                  (BaudRateOptions)     UART_115200bps, (BaudRateOptions)    UART_115200bps

                                          /*------------------------------------,------------------------------------*/
/* Lists of values applied to the     */  /*                          CHANNEL 0 ,                          CHANNEL 1 */
/* hardware initialization            */  /*------------------------------------,------------------------------------*/
#define MDBS_UART_ID_LIST                               IWM_01_MDBS_UART_ID_LIST
#define MDBS_UART_USED_PORT_LIST                          SERIAL_USB_SERVICE_INT,              (PortOptions) DONTCARE
#define MDBS_UART_ROUTED_PORT_LIST                       SERIAL_USB_SERVICE_UART,        (RoutedPortOptions) DONTCARE

#define MDBS_LORA_ID_LIST                               IWM_01_MDBS_LORA_ID_LIST

#define MDBS_CONFIG_LIST                                            MDBCFG_RS232,                     MDBCFG_LORA_DEV
#define MDBS_FRAME_FORMAT_LIST                                   NORMAL_RESPONSE,                     NORMAL_RESPONSE
#define MDBS_PIT_ID_LIST                                 IWM_01_MDBS_PIT_ID_LIST
#define MDBS_VUR_ID_LIST                                 IWM_01_MDBS_VUR_ID_LIST

/* These lists are used only if the   */
/* CONFIG is set as RS485, if the     */  /*------------------------------------,------------------------------------*/
/* configuration is not used, it must */  /*                          CHANNEL 0 ,                          CHANNEL 1 */
/* be declared as DONTCARE            */  /*------------------------------------,------------------------------------*/
#define MDBS_DIRECTION_GPIO_ID_LIST                             (uint8) DONTCARE,                    (uint8) DONTCARE
#define MDBS_DIRECTION_GPIO_PORT_LIST                           (uint8) DONTCARE,                    (uint8) DONTCARE
#define MDBS_DIRECTION_GPIO_PIN_LIST                            (uint8) DONTCARE,                    (uint8) DONTCARE

/* These lists are used only if the   */
/* CONFIG is set as RS232_USB, if the */  /*------------------------------------,------------------------------------*/
/* configuration is not used, it must */  /*                          CHANNEL 0 ,                          CHANNEL 1 */
/* be declared as DONTCARE            */  /*------------------------------------,------------------------------------*/
#define MDBS_RTS_GPIO_ID_LIST                                   (uint8) DONTCARE,                    (uint8) DONTCARE
#define MDBS_RTS_GPIO_PORT_LIST                                 (uint8) DONTCARE,                    (uint8) DONTCARE
#define MDBS_RTS_GPIO_PIN_LIST                                  (uint8) DONTCARE,                    (uint8) DONTCARE
#define MDBS_CTS_GPIO_ID_LIST                                   (uint8) DONTCARE,                    (uint8) DONTCARE
#define MDBS_CTS_GPIO_PORT_LIST                                 (uint8) DONTCARE,                    (uint8) DONTCARE
#define MDBS_CTS_GPIO_PIN_LIST                                  (uint8) DONTCARE,                    (uint8) DONTCARE


#define MDB_MASTER_LORA_SUPPORT                                             TRUE

/******************************************************************************/
/*        SELF DIAG SPECIFIC CONFIGURATION                                    */
/******************************************************************************/
/* Define below the PIT ID that the Self Diag Lib can use for itself.         */
#define SELF_DIAG_PIT_ID                                 IWM_01_SELF_DIAG_PIT_ID
/* Define below the time period for the analog acquisitions, in ms.           */
#define SELFDIAG_ACQ_PERIOD                                                 5000

//#define SELF_DIAG_BOARD_TEMP_CHANNEL                           ANALOG_TEMPSENSOR

/******************************************************************************/
/*        DIGIOK_COMPILE specific configuration                               */
/**************************************************************************** */
/* Functionalities Control                                                    */
#define DIGIOK_SIMPLE_OUTPUT                                DIGIOK_FUNC_DISABLED
#define DIGIOK_SIMPLE_INPUT                                  DIGIOK_FUNC_ENABLED
#define DIGIOK_LED_OUTPUT                                   DIGIOK_FUNC_DISABLED
#define DIGIOK_FREQUENCY_OUTPUT                             DIGIOK_FUNC_DISABLED
#define DIGIOK_PULSE_OUTPUT                                 DIGIOK_FUNC_DISABLED
#define DIGIOK_STATE_OUTPUT                                 DIGIOK_FUNC_DISABLED

/******************************************************************************/
/*        LED STATE MACHINE CONFIGURATION                                     */
/******************************************************************************/
#define STATUS_STATE_MACHINE_PIT                 IWM_01_STATUS_STATE_MACHINE_PIT

/******************************************************************************/
/*        POWER MANAGEMENT SPECIFIC CONFIGURATION                             */
/******************************************************************************/
/////////////////////////////////////
// Battery charger configurations  //
/////////////////////////////////////
#define CHARGER_AMOUNT                                                         1

#define CHARGER_GPIO_ID_LIST          (uint8)                           DONTCARE
#define CHARGER_GPIO_PIN_LIST         (uint8)                           DONTCARE
#define CHARGER_GPIO_PORT_LIST        (uint8)                           DONTCARE
#define CHARGER_GPIO_INIT_STATE_LIST  BATTERY_CONNECTED

#define CHARGER_IIC_ID_LIST                           IWM_01_CHARGER_IIC_ID_LIST
#define CHARGER_IIC_INTERFACE_LIST                                   I2C_BUS_INT
#define CHARGER_IIC_SDA_ROUTE_LIST                                   I2C_BUS_SDA
#define CHARGER_IIC_SCL_ROUTE_LIST                                   I2C_BUS_SCL
#define CHARGER_IIC_BAUDRATE_LIST                                    IIC_100kbps

#define CHARGER_PIT_ID                                     IWM_01_CHARGER_PIT_ID
/* Select below which are the Gpio levels that enable and disable the         */
/*  chargers. Library will follow there values when enabling/disabling them.  */
/* It follows the Gpio levels defined in the GPIO.h header file.              */
/* Possible values: GPIO_HIGH_LEVEL, GPIO_LOW_LEVEL, GPIO_HIGH_Z              */
/*                                    CHARGER ONE                             */
#define CHARGER_GPIO_ENABLE_LVL_LIST  GPIO_HIGH_Z,
#define CHARGER_GPIO_DISABLE_LVL_LIST GPIO_HIGH_LEVEL,
/* Select below the conversion factor that has to be applied to the info      */
/*  provided by the chargers, so that its electrical current value matches    */
/*  the desired format AND compensates errors due to circuit's impedance.     */
/* The current value is of 1.0, which basically disables this factor          */
#define CHARGER_CONV_FACTOR_LIST      1.0

/* Set below the charger's reading period, in seconds.                        */
#define BATT_UPDATE_PERIOD                                                     5

/* Select below the initial values for chargers and batteries                 */
#define POWER_CHARGER_1_INIT                                         BQ27411_G1C
#define POWER_CHARGER_2_INIT                                         BQ27411_G1C

#define POWER_BATTERY_1_INIT                                        BIK_06180063
#define POWER_BATTERY_2_INIT                                NO_BATTERY_CONNECTED

//////////////////////////////
// Generator configurations //
//////////////////////////////
#define GENERATOR_GPIO_ID                             (uint8)           DONTCARE
#define GENERATOR_GPIO_ENABLE_PIN                     (GPIO_PIN_list)   DONTCARE
#define GENERATOR_GPIO_ENABLE_PORT                    (GPIO_PORT_list)  DONTCARE

/******************************************************************************/
/*        ACCEL TILT LIBRARY SPECIFIC CONFIGURATION                           */
/******************************************************************************/
/* IDs ASSIGNMENT                                                             */
#define ACCEL_TILT_ALARM_ID_LIST                 IWM_01_ACCEL_TILT_ALARM_ID_LIST

/* ACCELEROMETER MANUAL ACQUISITION DEFINITIONS                               */
/* ACCEL_MAN_SAMPLING_T: Time, in seconds, between manual samples.            */
/*  If a value of zero is given, then manual sampling is disabled.            */
#define ACCEL_MAN_SAMPLING_T                                               14400  /*  4 hrs */
/* ACCEL_REF_REFRESH_T: Time, in seconds, between reference refresh events    */
/*  If a value of zero is given, then manual sampling is disabled.            */
#define ACCEL_REF_REFRESH_T                                                86400  /* 24 hrs */

/* N_SAMPLES_ATTEMPT - How many times the routine, per operation, will try to */
/*  acquire valid acceleration vectors. It'll average them and return it.     */
#define N_SAMPLES_ATTEMPT                                                      4
/* BAD_VECTOR_LOW_THRES - If the samples's module is lower than this value,   */
/*  the sample will be considered invalid                                     */
/* BAD_VECTOR_HIGH_THRES - If the samples's module is higher than this value, */
/*  the sample will be considered invalid                                     */
#define BAD_VECTOR_LOW_THRES                                                 900 /* mg */
#define BAD_VECTOR_HIGH_THRES                                               1100 /* mg */

/* TILT BLOCK DEFINITIONS                                                     */
/* Set below if the block functionality will be enabled. If it is, the        */
/*  definitions following it will configure it.                               */
/* NOTE: Block is no longer enabled after MMA8451 device v2.0                 */
#define ACCEL_ENABLE_BLOCK                                                 FALSE
/* ACCEL_BLOCK_PERIOD - If library enters block state, how long is its        */
/*  timeout value.                                                            */
#define ACCEL_BLOCK_PERIOD                                                    30 /* secs */
/* ACCEL_BLOCK_G_THRES - Set the delta value that will be checked against to  */
/*  consider a possible block event.                                          */
#define ACCEL_BLOCK_G_THRES                                                100.0 /* mg */
/* ACCEL_BLOCK_COUNT - If the threshold counts surpasses this value, the block*/
/*  event will trigger.                                                       */
#define ACCEL_BLOCK_COUNT                                                      4

/******************************************************************************/
/*        ACCELEROMETER MMA8451 SPECIFIC CONFIGURATION                        */
/******************************************************************************/
/* General Accelerometer Configuration                                        */
#define ACCEL_RESOLUTION_VALUE      /* Accel_Res_t */           Resolution_14bit
#define ACCEL_SCALE_VALUE           /* Accel_Scale_t */                 Scale_8g
#define ACCEL_DATA_RATE             /* Accel_ODR_t */                 ODR_12_5Hz
#define ACCEL_HIGHPASS_CUTOFF       /* Accel_HPFCutoff_t */          HPFC_SEL_01
#define ACCEL_ACTIVE_POWER_SCHEME   /* Accel_Scheme_t */     PMode_LNoise_LPower

/* Interrupt Definitions                                                      */
#define ACCEL_INT_DATAREADY         /* Accel_Int_t */               Int_Disabled
#define ACCEL_INT_IMPACT            /* Accel_Int_t */            Int_Enable_Int2
#define ACCEL_INT_MOVEMENT          /* Accel_Int_t */            Int_Enable_Int2

/* Motion Definitions                                                         */
/* These parameters will define the movement (tilt) interrupt                 */
#define ACCEL_MOVEMT_THRES                                                   150 /* mg */
#define ACCEL_MOVEMT_CNT                                                    1500 /* ms */

/* Transient Definitions                                                      */
/* These parameters will define the impact interrupt                          */
#define ACCEL_IMPACT_THRES                                                  3000 /* mg */
#define ACCEL_IMPACT_CNT                                                     500 /* ms */

/* Accelerometer's IIC Configuration                                          */
#define ACCEL_IIC_ID                /* uint8 */              IWM_01_ACCEL_IIC_ID
#define ACCEL_IIC_INTERFACE         /* IIC_InterfaceOptions */       I2C_BUS_INT
#define ACCEL_IIC_SDA_ROUTE         /* IIC_Routing  */               I2C_BUS_SDA
#define ACCEL_IIC_SCL_ROUTE         /* IIC_Routing  */               I2C_BUS_SCL
#define ACCEL_IIC_BAUDRATE          /* IIC_BaudRate */               IIC_100kbps

/* Accelerometer's PIT Configuration                                          */
#define ACCEL_PIT_ID_LIST                               IWM_01_ACCEL_PIT_ID_LIST

/* Accelerometer's Interrupt Pins                                             */
/* Note: If the interrupt pin is not used, set its ID with DONTCARE           */
#define ACCEL_INT1_GPIO_ID                                              DONTCARE
#define ACCEL_INT1_GPIO_PORT        /* GPIO_PORT_list */                DONTCARE
#define ACCEL_INT1_GPIO_PIN         /* GPIO_PIN_list */                 DONTCARE

#define ACCEL_INT2_GPIO_PORT        /* GPIO_PORT_list */          ACCEL_INT_PORT
#define ACCEL_INT2_GPIO_PIN         /* GPIO_PIN_list */            ACCEL_INT_PIN

/******************************************************************************/
/*        CALENDAR LIBRARY CONFIGURATION                                      */
/******************************************************************************/
/* RTC Alarm ID for calendar alarm feature */
#define CALENDAR_ALARM_ID_LIST                     IWM_01_CALENDAR_ALARM_ID_LIST

/* PIT IDs for Running Operation                                              */
#define CALENDAR_OPERATION_PIT_ID               IWM_01_CALENDAR_OPERATION_PIT_ID

/******************************************************************************/
/*        DEVICE RTC PSEUDO CONFIGURATION                                     */
/******************************************************************************/
/* Select below the maximum number of alarms that the device will handle.     */
#define DEV_RTC_NUMBER_OF_ALARMS                                               5

/* Select below the main PIT IDs for the RTC task                             */
#define DEV_RTC_PIT_ID_LIST                           IWM_01_DEV_RTC_PIT_ID_LIST

/******************************************************************************/
/*        DATALOGGER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/* Maximum available ID's.                                                    */
#define MAX_DLOG_ID_AVAILABLE                                                  4

/* Buffer length for write and read internal operations.                      */
#define MAX_DLOG_BUF_LENGTH                               ( SPI_BUF_LENGTH - 8 )

/******************************************************************************/
/*        STORAGE SPECIFIC CONFIGURATION                                      */
/******************************************************************************/
#define STORAGE_ID_NVV_0                                  IWM_01_EEPROM_ID_NVV_0
#define STORAGE_ID_NVV_1                                  IWM_01_EEPROM_ID_NVV_1
#define STORAGE_ID_MDB_0                                  IWM_01_EEPROM_ID_MDB_0
#define STORAGE_ID_MDB_1                                  IWM_01_EEPROM_ID_MDB_1

/* Default signature value.                                                   */
#define SETUP_DEF_SIGNATURE_VAL                                   (uint16)0xDAD1

/* Maximum block size.                                                        */
#define SETUP_STORAGE_BLOCK                               ( SPI_BUF_LENGTH - 8 )

/******************************************************************************/
/*        EEPROM CAT24M01 DEVICE SPECIFIC CONFIGURATION                       */
/******************************************************************************/
/* Set the ID limit                                                           */
#define EEP_CAT24M01_ID_LIMIT                                                 10

/* Set below the IIC parameters                                               */
#define EEP_CAT24M01_IIC_ID                           IWM_01_EEP_CAT24M01_IIC_ID
#define EEP_CAT24M01_IIC_INTERFACE                                   I2C_BUS_INT
#define EEP_CAT24M01_IIC_SDA                                         I2C_BUS_SDA
#define EEP_CAT24M01_IIC_SCL                                         I2C_BUS_SCL
#define EEP_CAT24M01_IIC_BAUDRATE                                    IIC_100kbps
/* Set below the PIT ID available for the device                              */
#define EEP_CAT24M01_PIT_ID                           IWM_01_EEP_CAT24M01_PIT_ID

/* Set below how many spare bytes will be given for each ID at their first    */
/*  initialization. By giving these bytes we provide a margin where the ID can*/
/*  increase its data usage without giving trouble to the device.             */
#define EEP_CAT24M01_SPARE_MEM_BYTES                                         256

/* Set below the data signature code for each ID below. If the ID's sign field*/
/*  doesn't match this value then the data is considered invalid.             */
#define EEP_CAT24M01_SIGN_CODE                                              0xAB

/******************************************************************************/
/*        RAM MEMORY DEVICE SPECIFIC CONFIGURATION                            */
/******************************************************************************/
#define EEP_MEM_RAM_ID_LIMIT                                                  10
#define EEP_MEM_RAM_MEM_SIZE                                                 128

/******************************************************************************/
/*        RADIO SX1276 DEVICE SPECIFIC CONFIGURATION                          */
/******************************************************************************/
#define LORA_DEV_ID                                           IWM_01_LORA_DEV_ID
#define LORA_TIMER_ID                                       IWM_01_LORA_TIMER_ID
#define LORA_GPIO_RST_ID                                 IWM_01_LORA_GPIO_RST_ID

#define LORA_PARAM_BW                                           Bandwidth_125Khz
#define LORA_PARAM_CD                                             CodindRate_4_5
#define LORA_PARAM_FREQ                                               Freq915Mhz
#define LORA_PARAM_SF                                                        SF7
#define LORA_PARAM_PWR                                                  Pwr20dbm

#define LORA_PIN_RST                                        BOARD_LORA_RESET_PIN
#define LORA_PORT_RST                                      BOARD_LORA_RESET_PORT

#define LORA_SPI_PORT                                             BOARD_LORA_SPI
#define LORA_SPI_BAUDRATE                                            SPI_500kbps
#define LORA_SPI_CS_PORT                                      BOART_LORA_CS_PORT
#define LORA_SPI_CS_PIN                                        BOART_LORA_CS_PIN
#define LORA_SPI_PHASE                                          PHA_LEADING_EDGE
#define LORA_SPI_POLARITY                                         POL_ACTIVE_LOW
#define LORA_SPI_CS_POLARITY                                      POL_ACTIVE_LOW

/******************************************************************************/
/*        LIB HF READER SPECIFIC CONFIGURATION                                */
/******************************************************************************/
#define LIB_HF_READER_PIT_ID                         IWM_01_LIB_HF_READER_PIT_ID

#define HF_READER_SWITCH_ON_OFF_PIN_ID     IWM_01_HF_READER_SWITCH_ON_OFF_PIN_ID
#define HF_READER_SWITCH_ON_OFF_PORT                     BOARD_HF_READER_ON_PORT
#define HF_READER_SWITCH_ON_OFF_PIN                       BOARD_HF_READER_ON_PIN

#define LIB_HF_READER_WAIT_TIME                                              250
#define LIB_HF_READER_READ_RETRY                                               5

/******************************************************************************/
/*        HF READER CR95HF SPECIFIC CONFIGURATION                             */
/******************************************************************************/
#define DEV_HF_Reader_PIT_ID                         IWM_01_DEV_HF_Reader_PIT_ID
#define DEV_HF_Reader_Sleep_Time                                            250

#define Dev_HF_Reader_MAX_ID_LIMIT                                             1
#define DEV_HF_READER_ID                                 IWM_01_DEV_HF_READER_ID

#define DEV_HF_Reader_UART_ID                       IWM_01_DEV_HF_Reader_UART_ID
#define DEV_HF_Reader_UART_PORT                             SERIAL_HF_READER_INT
#define DEV_HF_Reader_UART_ROUTEDPORT                      SERIAL_HF_READER_UART
#define DEV_HF_Reader_UART_BAUDRATE                                UART_57600bps
#define DEV_HF_Reader_UART_TimeOut                                            20

/******************************************************************************/
/*        UPLOADER SPECIFIC CONFIGURATION                                     */
/******************************************************************************/
//#define ENABLE_SELF_PROGRAMMING_ON_FLASH

/* -> Flash Addressing definitions for SPF Mode */
#ifdef ENABLE_SELF_PROGRAMMING_ON_FLASH
  /* WARNING: The values below should be aligned with the memory allocation   */
  /*  on the Linker File!                                                     */
  /* Set below the flash sector's start address for the main boot table.      */
  #define FLASH_MAIN_BOOT_TABLE_ADDRESS                  ( (uint32) 0x00002000 ) /* Start of m_bootdatamain  */
  /* Set below the flash sector's start address for the copy boot table.      */
  #define FLASH_COPY_BOOT_TABLE_ADDRESS                  ( (uint32) 0x00082000 ) /* Start of m_bootdatacopy  */
  /* Set below the start and end addresses for the copy program data          */
  #define FLASH_COPY_PROGRAM_START_ADDRESS               ( (uint32) 0x00083000 ) /* Start of m_programbank   */
  #define FLASH_COPY_PROGRAM_END_ADDRESS                 ( (uint32) 0x000F9000 ) /* End of m_programbank     */
  /* Set below the start and end addresses for the original program data      */
  #define FLASH_ORIG_PROGRAM_START_ADDRESS               ( (uint32) 0x00003000 ) /* Interrupt vector         */
  #define FLASH_ORIG_PROGRAM_END_ADDRESS                 ( (uint32) 0x00079400 ) /* End of 'm_text'          */

  /* -> IDs */
  /* Define below the Internal Flash IDs Allocated for this library */
  #define UPLOADER_INTERNAL_FLASH_COPYBOOT_ID                                   IWM_01_UPLOADER_INTERNAL_FLASH_COPYBOOT_ID
  #define UPLOADER_INTERNAL_FLASH_MAINBOOT_ID                                   IWM_01_UPLOADER_INTERNAL_FLASH_MAINBOOT_ID
  #define UPLOADER_INTERNAL_FLASH_COPYPROGRAM_ID                                IWM_01_UPLOADER_INTERNAL_FLASH_COPYPROGRAM_ID
#endif

/* Define below the PIT ID Allocated for this library */
#define UPLOADER_PIT_ID                                   IWM_01_UPLOADER_PIT_ID

/******************************************************************************/
/*        INTERNAL FLASH SPECIFIC CONFIGURATION                               */
/******************************************************************************/
/* Select below how many IDs the driver will support                          */
/* Note: Current datalogger library will use Flash IDs 0, 1, 5 and 6!         */
#define INTERNAL_FLASH_ID_COUNT                                               7u
/* Select below the relative addressing start address and limit               */
/* Remember to configure the Linker file to allocate some data flash!         */
#define INTERNAL_FLASH_REL_ADDR_START                                 0x0007A000
#define INTERNAL_FLASH_REL_ADDR_END                                   0x00082000

/******************************************************************************/
/*        SYSTEM MANAGEMENT SPECIFIC CONFIGURATION                            */
/******************************************************************************/
/* Select below the sleep diagnostic mode. For further selection, check the   */
/*  library's link header file.                                               */
/* Possible values: SYSMNG_SLP_DIAG_DISABLED, SYSMNG_SLP_DIAG_SIMPLE,         */
/*                  SYSMNG_SLP_DIAG_FULL                                      */
#define SYSMNG_SLP_DIAG                                 SYSMNG_SLP_DIAG_DISABLED

/* PIT IDs for Power Management                                               */
#define POWER_PIT_ID_LIST                               IWM_01_POWER_PIT_ID_LIST

/* Library doesn't handle the pulse driver in this project.                   */
#define SYSMNGT_MANAGE_PULSES                                              FALSE

/******************************************************************************/
/*        SPI DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
#define SPI_BUF_LENGTH                                                       256

/******************************************************************************/
/*        PIT DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/* Select below the main PIT index. THE DRIVER WILL USE THE SELECTED ONE PLUS */
/*  THE ONE BELOW IT. They will be used in chained mode.                      */
#define PIT_PERIPHERAL                                                         3  /* PIT3 and PIT2 will be used */
/* Select below the maximum number of IDs that the driver will accept.        */
#define PIT_MAX_TIMERS_AVAILABLE                                IWM_01_PIT_COUNT

/******************************************************************************/
/*        ADC DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
#define MAX_ADC_CHANNELS_AVAILABLE                                             8

/******************************************************************************/
/*        VUR SPECIFIC CONFIGURATION                                          */
/******************************************************************************/
/* Limit the VUR's queue size, as RAM is short and it is not expected to have */
/*  much activity at the same time.                                           */
#define VUR_SIZE                                                              32

#endif

/******************************************************************************/
/*        DEBUG SPECIFIC CONFIGURATION                                        */
/******************************************************************************/
/* DEBUG Definitions are outside the header's protection block to guarantee that it is visible to all libs */
/* Select below if the debug functionalities will be enabled: DEBUG_ENABLED, DEBUG_DISABLED */
#define DEBUG_MODE                                                DEBUG_DISABLED
//#define ENABLE_UART_DEBUG

#define UART_DEBUG_ID                                       IWM_01_UART_DEBUG_ID
#define UART_DEBUG_PORT                                           DEBUG_UART_INT
#define UART_DEBUG_ROUTED                                       DEBUG_UART_ROUTE
#define UART_DEBUG_BAUDRATE                                       UART_115200bps

#define LED_RED_GPIO_ID                                   IWM_01_LED_RED_GPIO_ID
#define LED_RED_GPIO_PIN                                                   PIN_5
#define LED_RED_GPIO_PORT                                                 PORT_A

#define LED_GREEN_GPIO_ID                               IWM_01_LED_GREEN_GPIO_ID
#define LED_GREEN_GPIO_PIN                                                 PIN_2
#define LED_GREEN_GPIO_PORT                                               PORT_B

#define LED_YELLOW_GPIO_ID                               IWM_01_LED_BLUE_GPIO_ID
#define LED_YELLOW_GPIO_PIN                                               PIN_12
#define LED_YELLOW_GPIO_PORT                                              PORT_A

#define DEBUG_PIT_ID                                         IWM_01_DEBUG_PIT_ID
