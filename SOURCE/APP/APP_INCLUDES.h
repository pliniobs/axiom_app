/* *****************************************************************************
 FILE_NAME:     APP_INCLUDES.h
 DESCRIPTION:   Application header file for AVM-01 SAMPLER project
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: jul/2017
 VERSION:       1.0
***************************************************************************** */

/* Library access definition                                                  */
#ifndef SOURCES_APP_INCLUDES_H_
#define SOURCES_APP_INCLUDES_H_

/* Application specific includes                                              */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


#include "APP.h"
#include "Debug.h"

///* Library includes                                                           */
//#include "CALENDAR_Link.h"
//#include "MDBS_Link.h"
//#include "STORAGE_Link.h"
//#include "DATALOGGER_Link.h"
//#include "SELF_DIAG_Link.h"
//#include "ACCEL_TILT_Link.h"
//#include "SYS_MNG_Link.h"
//#include "DIGITAL_IO_Link.h"
//
//
///* Private Application includes                                               */
//
///* Map includes                                                               */
//#include "VARMAP.h"
//#include "MDBS_MAP.h"
//#include "SYSTEM_MAP.h"
///* Support routine includes                                                   */
//
//
//////////////////////////////
////   Compiler Warnings    //
//////////////////////////////
//#if CHECK_BRANCH(CALENDAR_BRANCH_MASTER) || CHECK_VERSION(CALENDAR_VER, 5, 13)
//  #error "Calendar branch or version error!!"
//#endif
//
//#if CHECK_BRANCH(MDB_SLAVE_BRANCH_MASTER) || CHECK_VERSION(MDB_SLAVE_VER, 4, 17)
//  #error "MDBS branch or version error!!"
//#endif
//
//#if CHECK_BRANCH(STORAGE_BRANCH_MASTER) || CHECK_VERSION(STORAGE_VER, 2, 0)
//  #error "Storage branch or version error!!"
//#endif
//
////#if CHECK_BRANCH(DATALOGGER_BRANCH_DEV) || CHECK_VERSION(DATALOGGER_VER, 2, 0)
////  #error "Datalogger branch or version error!!"
////#endif
//
//#if CHECK_BRANCH(SELF_DIAG_BRANCH_MASTER) || CHECK_VERSION(SELF_DIAG_VER, 3, 2)
//  #error "Self Diag branch or version error!!"
//#endif
//
//#if CHECK_BRANCH(ACCEL_TILT_BRANCH_MASTER) || CHECK_VERSION(ACCEL_TILT_VER, 3, 1)
//  #error "Accel Tilt branch or version error!!"
//#endif
//
//#if CHECK_BRANCH(SYSMNGT_BRANCH_MASTER) || CHECK_VERSION(SYSMNGT_VER, 6, 0)
//  #error "SysMng branch or version error!!"
//#endif


#endif /* SOURCES_APP_APP_INCLUDES_H_ */
