#ifndef SETUP_FREEDOM_VARS_H_INCLUDE
#define SETUP_FREEDOM_VARS_H_INCLUDE

/******************************************************************************/
/*        DEFINES, ENUMS, STRUCTURES                                          */
/******************************************************************************/
/* Definition to a value to be used                                           */
/* as an irrelevant filling                                                   */
#define DONTCARE                                                      0xFFFFFFFF
#define NULL_VALUE                                                             0
#define NaN                                                   (float)(0.0F/0.0F)

/******************************************************************************/
/*        PIT DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/***** Used Peripheral *****/
/* Select below the main PIT index. THE DRIVER WILL USE THE SELECTED ONE PLUS */
/*  THE ONE BELOW IT. They will be used in chained mode.                      */
#define PIT_PERIPHERAL                                                         3  /* PIT3 and PIT2 will be used */
/* Select below the maximum number of IDs that the driver will accept.        */
#define PIT_MAX_TIMERS_AVAILABLE                                              25


/******************************************************************************/
/*        SPI DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/* Define below the buffer size for the SPI transactions. Main points:        */
/* - Two buffers with this size will be allocated for each SPI peripheral     */
/* - Maximum send size is equal to this length.                               */
/* - Maximum receive size is equal to this length minus the command length.   */
#define SPI_BUF_LENGTH                                                      2000
/* Define below how many IDs the driver will support.                         */
#define SPI_MAX_ID_LIMIT                                                       4


/******************************************************************************
 *                  LIB ETHERCAT
 *****************************************************************************/

#define ENABLE_ARM
#define DEVELOPMENT_OD


/*ID's                                                                        */
#define SPI_ETHERCAT_ID                                              ETHC_SPI_ID
#define SPI_ETHERCAT_CS_PIN_ID                               GPIO_CS_ETHERCAT_ID
#define ETHERCAT_PIT_ID                                      ETHC_GENERIC_PIT_ID
#define ETHERCAT_TASK_UPDATE_PDO_PIT_ID              ETHC_TASK_UPDATE_PDO_PIT_ID


/*METROLOGICAL DEFINITIONS                                                    */


/*
 #define SLOT_QUANTITY -> how many slot there are in slave
 #define SLOT_DATA_LENGTH -> how many bytes there are in pdo area by slot.

 #define MFE_DATA_LENGTH -> source module data length

 #define DIAGNOSTIC_LENGTH -> diagnostic data length

 #define ALARM_LENGTH -> Alarm data length

 #define METROLOGICAL_LENGTH -> Metrological data length

 #define LENGTH_PDO_AREA ->Total length in pdo area

 for more information, consult EtherCat Specification document on page 14.    */

#define SLOT_QUANTITY                                                         10
#define SLOT_DATA_LENGTH                                                      39
#define MFE_DATA_LENGTH                                                       56
#define DIAGNOSTIC_LENGTH                                                      3
#define ALARM_LENGTH                                                           3
#define METROLOGICAL_LENGTH                                                  150

#define LENGTH_PDO_AREA  (SLOT_QUANTITY*SLOT_DATA_LENGTH) + MFE_DATA_LENGTH    \
                        + DIAGNOSTIC_LENGTH + ALARM_LENGTH + METROLOGICAL_LENGTH

/*OUTPUT INPUT AREA                                                           */
#define DATA_LENGTH_OUTPUT_AREA                                             0x00
#define DATA_LENGTH_INPUT_AREA                    SLOT_DATA_LENGTH*SLOT_QUANTITY

/*SPI CONFIGURATION                                                           */
#define ETHERCAT_SPI_PORT                        SPI0_AT_PORT_D_SCK1_SOUT2_SIN3
#define ETHERCAT_SPI_PORT_PIN_CS                                  CS_PORTC_PIN0
#define ETHERCAT_SPI_CS_POL                                      POL_ACTIVE_LOW
#define ETHERCAT_SPI_PHA                                      PHA_TRAILING_EDGE
#define ETHERCAT_SPI_POL                                         POL_ACTIVE_LOW
#define ETHERCAT_SPI_BAUDRATE                                         SPI_5Mbps
/**GPIO CHIP SECELT                                                          */
#define ETHERCAT_SPI_CS_PIN                                               PIN_0
#define ETHERCAT_SPI_CS_PORT                                             PORT_D

/******************************************************************************/
/*        SPI DRIVER IDs                                                      */
/******************************************************************************/
#define ETHC_SPI_ID                                                           1


/******************************************************************************/
/*        PIT DRIVER IDs                                                      */
/******************************************************************************/
#define ETHC_GENERIC_PIT_ID                                                   1
#define ETHC_TASK_UPDATE_PDO_PIT_ID                                           4


/******************************************************************************/
/*        GPIO DRIVER IDs                                                     */
/******************************************************************************/
#define GPIO_CS_ETHERCAT_ID                                                   1

//#define GPIO_ID                                                              20


#endif
/******************************************************************************/
/*        DEBUG SPECIFIC CONFIGURATION                                        */
/******************************************************************************/
/* DEBUG Definitions are outside the header's protection block to guarantee that it is visible to all libs */
#ifdef IMPORT_SETUP_DEBUG
  /* Select below if the debug functionalities will be enabled: DEBUG_ENABLED, DEBUG_DISABLED */
//  #define DEBUG_MODE                                               DEBUG_ENABLED
//
//  #define LED_RED_GPIO_ID                                                     27
//  #define LED_RED_GPIO_PIN                                                PIN_22  /* RED LED from K64 Freedom board */
//  #define LED_RED_GPIO_PORT                                               PORT_B
//
//  #define LED_GREEN_GPIO_ID                                                   28
//  #define LED_GREEN_GPIO_PIN                                              PIN_16  /* Generic debug pin */
//  #define LED_GREEN_GPIO_PORT                                             PORT_C
//
//  #define LED_YELLOW_GPIO_ID                                                  29
//  #define LED_YELLOW_GPIO_PIN                                              PIN_4  /* Used to supply the RTC device in the mod Freedom board */
//  #define LED_YELLOW_GPIO_PORT                                            PORT_C
//
//  #define DEBUG_PIT_ID                                                        20


/******************************************************************************/
/*        SETUP FILE SELECTION                                                */
/******************************************************************************/
#if CURRENT_PLATFORM == PLATFORM_DEVELOPMENT
  #include "SETUP_DEVELOPMENT.h"
#else
  #include "SETUP_RELEASE.h"
#endif

#endif
